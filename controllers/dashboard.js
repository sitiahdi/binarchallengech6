const express = require("express");
const router = express();

const {user_game, user_game_biodata, user_game_histories} = require("../models");

router.use(express.Router());

router.get("/dashboard", (req, res) =>{
  user_game.findAll().then((users) => {
    console.log(users)
    res.render("dashboard", {
        users
    })  
  })  
})

router.post("/post-user", (req, res) => {
  const params = {
    username: req.body.username,
    password: req.body.password,
    email: req.body.email,
    fullname: req.body.fullname,
    age: req.body.age,
    address: req.body.address
  };
    let score = Math.floor(Math.random() * 100) + 1;
    let play = Math.floor(Math.random() * 100) + 1;
    let userid = Math.floor(Math.random() * 100) + 1;
    user_game.create({
        user_id: userid,
        username: params.username,
        password: params.password,
        email: params.email
    }).then((value) => {
      res.redirect("dashboard")
    })
})

module.exports = router;


