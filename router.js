const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.render('homepage');
})

router.get('/game', (req, res) => {
    res.render('game');
})

router.get('/dashboard', (req, res) => {
    res.render('dashboard');
})

router.get('/login', (req, res) => {
    res.render('login');
})

const db = require('./DB/users.json');

router.get("/ceknamauser", (req, res) => {
    res.status(200).json(db);
})

router.get("/ceknamauser/:id", (req, res) => {
  const user = db.find((item) => item.id === +req.params.id);
  if (user) {
    res.status(200).json(user);
  } else {
    res.status(200).send("ID not found");
  }
});

router.post("/post-login", (req, res) => {
    let request = req.body;
    let idUser = db;
    for (i = 0; i < idUser.length; i++) {
        const element = idUser[i];
        if ((request.username === "admin") && (request.password === "123456")) {
            res.status(200)
            res.redirect("/dashboard")
        } else if ((request.username === element.username) && (request.password === element.password)) {
            res.status(200)
            res.redirect("/game")
        } else {
            res.status(401)
            res.send("Mohon Daftar Dulu")
        }
    }
})

module.exports = router