let play1 = document.getElementById("p1");
let play2 = document.getElementById("p2");
let play3 = document.getElementById("p3");
let result = document.getElementById("result");
let tombol = document.getElementById("tombol");

let tempResult = 1;
result.style.color = "antiquewhite";

class Player{
    constructor(name, isHuman){
        this.name = name;
        this.isHuman = isHuman;
    }
    random(){
        let value = [1, 2, 3]; 
        return value[Math.floor(Math.random()*value.length)]; 
    }
    reset(){ 
        this.resetwarna(); 
        tempResult = 0; 
        result.textContent = "VS" 
        result.style.color = "antiquewhite";
    } 
    resetwarna(){ 
        document.getElementById("p1").style.background = "transparent";  
        document.getElementById("p2").style.background = "transparent"; 
        document.getElementById("p3").style.background = "transparent"; 
        document.getElementById("c1").style.background = "transparent";  
        document.getElementById("c2").style.background = "transparent";  
        document.getElementById("c3").style.background = "transparent";  
     }

}

class mainPlayer extends Player{
    constructor(name, isHuman, pilihan){
        super(name, isHuman)
        this.pilihan = pilihan;
    }
    clicking(isiPlay){ 
        this.resetwarna();
        let isiCom = this.random()
        document.getElementById("p" + isiPlay).style.background = "grey";
        console.log(isiPlay); 
        console.log(isiCom);
        result.textContent = getWinner(isiPlay, isiCom);
        document.getElementById("c" + isiCom).style.background = "grey";
    }
}

class Com extends Player{
    constructor(name, isHuman, hasil){
        super(name, isHuman)
        this.hasil = hasil;
    }
}

let playerHuman = new mainPlayer("Siti", isHuman=true);
let playerCom = new Com("Robot", isHuman=false);

play1.addEventListener("click", () => playerHuman.clicking(1)); 
play2.addEventListener("click", () => playerHuman.clicking(2)); 
play3.addEventListener("click", () => playerHuman.clicking(3));
tombol.addEventListener("click", () => playerHuman.reset());

function getWinner(firstPlayer, secondPlayer){
    var status;
    var warna;
    if(firstPlayer=="1" && secondPlayer=="3"){ status = "WIN"; warna = "#4C9654"; }
    else if(firstPlayer=="1" && secondPlayer=="2"){ status = "LOSE"; warna = "red"; }
    else if(firstPlayer=="2" && secondPlayer=="1"){ status = "WIN"; warna = "#4C9654"; }
    else if(firstPlayer=="2" && secondPlayer=="3"){ status = "LOSE"; warna = "red"; }
    else if(firstPlayer=="3" && secondPlayer=="1"){ status = "LOSE"; warna = "red"; }
    else if(firstPlayer=="3" && secondPlayer=="2"){ status = "WIN"; warna = "#4C9654"; }
    else { status = "DRAW"; warna = "black"; }
    document.getElementById("result").style.color = warna;
    return status; 
}
