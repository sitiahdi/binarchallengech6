const express = require('express');
const app = express();
const port = 3000;

const importRouter = require('./router.js');
const importDashboard = require('./controllers/dashboard.js');

const logger = (req, res, next) => {
    console.log(`${req.method} ${req.url}`)
    next()
}

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use('/',importRouter);
app.use('/',importDashboard);

app.use(logger);

app.listen(port, () => {
  console.log(`You are now listening to port ${port}`);
})